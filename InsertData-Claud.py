import pymysql

# Database connection parameters
db_params = {
    'host': 'your-rds-endpoint.rds.amazonaws.com',
    'user': 'your_username',
    'password': 'your_password',
    'database': 'your_database_name',
    'port': 3306
}

# Connect to the database
try:
    conn = pymysql.connect(**db_params)
    cursor = conn.cursor()

    # SQL query to insert a record into the contact table
    sql = """
    INSERT INTO contact (first_name, last_name, email, phone)
    VALUES (%s, %s, %s, %s)
    """

    # Sample data to insert
    data = ('John', 'Doe', 'john.doe@example.com', '1234567890')

    # Execute the query
    cursor.execute(sql, data)

    # Commit the transaction
    conn.commit()

    print("Record inserted successfully")

except pymysql.Error as e:
    print(f"Error connecting to the database: {e}")

finally:
    # Close the cursor and connection
    if 'cursor' in locals():
        cursor.close()
    if 'conn' in locals():
        conn.close()