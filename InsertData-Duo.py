//GitLab Duo : Generate a python code to connect to AWS RDS and insert the record in a contact table

import boto3
from botocore.exceptions import ClientError

# Connect to AWS RDS
rds = boto3.client('rds')

# Define the database connection details
db_host = 'your-rds-endpoint.amazonaws.com'
db_name = 'your-database-name'
db_user = 'your-database-username'
db_password = 'your-database-password'

# Define the table and column names
table_name = 'contacts'
columns = ['name', 'email', 'phone']

# Function to insert a new record into the contacts table
def insert_contact(name, email, phone):
    try:
        # Create a connection to the database
        conn = rds.connect_to_database(
            DBHostname=db_host,
            DBName=db_name,
            DBUser=db_user,
            DBPassword=db_password
        )

        # Prepare the SQL query
        sql = f"INSERT INTO {table_name} ({', '.join(columns)}) VALUES (%s, %s, %s)"
        values = (name, email, phone)

        # Execute the query
        with conn.cursor() as cursor:
            cursor.execute(sql, values)
            conn.commit()

        print(f"Inserted new contact: {name}, {email}, {phone}")

    except ClientError as e:
        print(f"Error inserting contact: {e}")

    finally:
        # Close the database connection
        conn.close()
