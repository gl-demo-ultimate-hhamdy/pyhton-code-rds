import psycopg2
from psycopg2 import sql

# Database connection parameters
db_params = {
    'dbname': 'your_db_name',
    'user': 'your_db_user',
    'password': 'your_db_password',
    'host': 'your_db_host',  # For RDS, this will be the endpoint URL
    'port': 'your_db_port'   # Default is 5432 for PostgreSQL
}

# Contact information to be inserted
contact_info = {
    'name': 'John Doe',
    'email': 'john.doe@example.com',
    'phone': '123-456-7890'
}

try:
    # Establish the database connection
    connection = psycopg2.connect(**db_params)
    cursor = connection.cursor()
    
    # Insert query
    insert_query = sql.SQL("""
        INSERT INTO contact (name, email, phone)
        VALUES (%s, %s, %s)
    """)
    
    # Execute the insert query
    cursor.execute(insert_query, (contact_info['name'], contact_info['email'], contact_info['phone']))
    
    # Commit the transaction
    connection.commit()
    
    print("Record inserted successfully")
    
except Exception as error:
    print(f"Error inserting record: {error}")
    
finally:
    # Close the cursor and connection
    if cursor:
        cursor.close()
    if connection:
        connection.close()
